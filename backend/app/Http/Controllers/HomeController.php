<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Translation;
use App\Http\Resources\TranslationResource;

class HomeController extends Controller
{
    public function getTranslations()
    {
        return response()->json(['data' => TranslationResource::collection(Translation::inRandomOrder()->get())]);
    }
}
